package com.example.plugins

import com.example.dao.DAOFacadeImpl
import com.example.models.Article
import com.example.service.ArticleService
import io.ktor.server.routing.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import org.koin.ktor.ext.inject
import kotlin.system.measureTimeMillis

fun Application.configureRouting() {
    val service :  ArticleService by inject()
    val daoFacade : DAOFacadeImpl by inject()

    routing {

        get("/") {
            val time = measureTimeMillis {
                val dataMap = mutableMapOf<String,Article>()
                for (i in 1..500000) {
                    var dataArt =  Article( i.toInt(),i.toString(),i.toString())
                    dataMap[i.toString()] = dataArt
                }
               // dataMap.clear()
            }
            print("Got result after $time ms.")
            call.respond("hello")
        }

        route("articles") {
            get {
               // val time = measureTimeMillis {
                    call.respond(service.getAllArticles())
                    //call.respond(daoFacade.getAllArticles())
             /*   var data = transaction {
                    ArticleEntity.all().map(ArticleEntity::toArticle)
                }*/
                //call.respond(data)

               // call.respond(daoFacade.allArticles())
               // }
                //print("Got result after $time ms.")
            }
            get("/allArticlesCount"){
                    call.respond(daoFacade.getAllArticlesCount())
            }
           /* get("{id}") {
                val id = call.parameters.getOrFail<Int>("id").toInt()
                val time = measureTimeMillis {
                    call.respond(   mapOf("articles" to service.article(id)))
                }
                print("Got result after $time ms.")
            }*/
        }
    }


}
