package com.example.dao

import com.example.models.Article
import com.example.models.ArticleEntity
import com.example.models.Articles
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SizedIterable
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.koin.core.component.KoinComponent

class DAOFacadeImpl : DAOFacade, KoinComponent {

   /* private fun resultRowToArticle(row: ResultRow) = Article(
            id = row[Articles.id],
            title = row[Articles.title],
            body = row[Articles.body],
    )
    override fun allArticles(): List<Article> =
        transaction {
            Articles.selectAll().map(::resultRowToArticle)
        }*/


    override fun getAllArticles(): Iterable<Article> = transaction {
        ArticleEntity.all().map(ArticleEntity::toArticle)
    }

    override fun getAllArticlesCount(): Long = transaction {  ArticleEntity.all().count() }


    /*    override fun getAllArticles(): SizedIterable<ArticleEntity> = transaction {
            // ArticleEntity.all().map(ArticleEntity::toArticle)
            ArticleEntity.all()
        }*/

    /* override suspend fun allArticles(): List<Article> =
         dbQuery { Articles.selectAll().map(::resultRowToArticle) } */



    //override fun article(id: Int): Article? =
           // transaction {
      //      ArticleEntity.searchQuery("").
           /* Articles
                    .select { Articles.id eq id }
                    .map ( :: resultRowToArticle)
                    .singleOrNull()*/
       // }

   /* override suspend fun addNewArticle(title: String, body: String): Article? {
        TODO("Not yet implemented")
    }

    override suspend fun editArticle(id: Int, title: String, body: String): Boolean {
        TODO("Not yet implemented")
    }

    override suspend fun deleteArticle(id: Int): Boolean {
        TODO("Not yet implemented")
    }*/

}