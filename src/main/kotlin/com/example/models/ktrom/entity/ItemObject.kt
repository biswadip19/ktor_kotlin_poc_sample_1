package com.example.models.ktrom.entity


import org.ktorm.schema.Table
import org.ktorm.schema.int
import org.ktorm.schema.varchar

object ItemEntity :Table<Item>("items") {
    val itemid = int("itemid").primaryKey().bindTo { it.itemid }
    val name = varchar("name").bindTo { it.name }
    val id=int("id").references(ArticlesEntity){it.id}

}