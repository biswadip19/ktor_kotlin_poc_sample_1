package com.example.models.ktrom.entity


import org.ktorm.entity.Entity


interface Article:Entity<Article>{
    val id:Int
    val title:String
    val body:String
}


