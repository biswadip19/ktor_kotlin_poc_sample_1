package com.example.models

import kotlinx.serialization.Serializable
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.Table

//Approach#1
/*object Articles : Table() {
    val id = integer("id").autoIncrement()
    val title = varchar("title", 128)
    val body = varchar("body", 1024)
    override val primaryKey = PrimaryKey(id)
}*/

//Approach#2
object Articles : IntIdTable() {
    val title = varchar("title", 128)
    val body = varchar("body", 1024)
}

class ArticleEntity(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<ArticleEntity>(Articles)
    var title by Articles.title
    var body by Articles.body
    override fun toString(): String = "Article($title, $body)"
    fun toArticle() = Article(id.value, title, body)
}

@Serializable
data class Article(
    val id: Int,
    val title: String,
    val body: String
)
