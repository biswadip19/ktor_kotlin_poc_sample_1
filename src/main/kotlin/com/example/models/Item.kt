package com.example.models

import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Table


@Serializable
data class Item(val itemid: Int, val name: String ,val id:Int)

object Items : Table() {
    //object Articles:Table<Nothing>{
    val itemid = integer("itemid").autoIncrement()
    val name = varchar("name", 128)
    val id= integer("id")
    override val primaryKey = PrimaryKey(itemid)
}
