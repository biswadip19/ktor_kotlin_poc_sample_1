package com.example.models

import kotlinx.serialization.Serializable

@Serializable
data class ItemResponse<T>(
    val data:T
)