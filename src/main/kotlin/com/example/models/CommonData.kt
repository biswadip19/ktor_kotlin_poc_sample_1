package com.example.model

import kotlinx.serialization.Serializable
import org.ktorm.dsl.QueryRowSet

@Serializable
data class CommonDatas (val id: Int, val itemName: String, val articleTitle : String)

class CommonData {
    @Serializable
    val commonDataList = ArrayList<CommonDatas>()

    fun addToCommonDataList(row: QueryRowSet) = commonDataList.add(CommonDatas(row.getInt(1),
        row.getString(2)?:"",row.getString(3)?:""))

    @JvmName("getCommonDataList1")
    fun getCommonDataList():ArrayList<CommonDatas> = commonDataList

}