/*
package com.example.hikariconfig

import com.typesafe.config.ConfigFactory
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.ktor.server.config.*
import java.sql.Connection
import java.sql.SQLException


object HikariCustDataSource {
    private val config: HikariConfig = HikariConfig()
    private var ds: HikariDataSource? = null
    private val appConfig = HoconApplicationConfig(ConfigFactory.load())
    private val dbUrl = appConfig.property("db.jdbcUrl").getString()
    private val dbUser =  appConfig.property("db.dbUser").getString()
    private val dbPassword = appConfig.property("db.dbPassword").getString()
    fun init() {
        config.setJdbcUrl("$dbUrl")
        config.setUsername("$dbUser")
        config.setPassword("$dbPassword")
        config.addDataSourceProperty("cachePrepStmts", "true")
        config.addDataSourceProperty("prepStmtCacheSize", "250")
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048")
        ds = HikariDataSource(config);
    }
    @get:Throws(SQLException::class)
    val connection: Connection
        get() = ds!!.getConnection()

}*/
