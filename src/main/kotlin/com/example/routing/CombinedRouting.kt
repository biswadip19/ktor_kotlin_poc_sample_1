package com.example.routing


import com.example.dao.DAOFacadeImpl
import com.example.model.CommonData
import com.example.models.ktrom.entity.ArticlesEntity
import com.example.models.ktrom.entity.ItemEntity
import com.example.service.ArticleService
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.util.*
import org.koin.ktor.ext.inject
import org.ktorm.database.Database
import org.ktorm.dsl.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory

fun Application.configureCombineRouters( ktromHikariDatabaseConn : Database ) {

    val LOGGER: Logger = LoggerFactory.getLogger(Application::class.simpleName)
    val service :  ArticleService by inject()
    val daoFacade : DAOFacadeImpl by inject()

    routing {

        route("/combinedItem"){
            get {
                try{
                    var commonData = CommonData()
                    val joined=ktromHikariDatabaseConn
                        .from(ItemEntity)
                        .innerJoin(ArticlesEntity , on= ItemEntity.id eq ArticlesEntity.id)
                        .select( ItemEntity.id, ItemEntity.name, ArticlesEntity.title)
                        .where(ArticlesEntity.id eq 1)
                        .forEach { row ->
                            commonData.addToCommonDataList(row)
                            println("${row.getString(1)}")
                        }
                    call.respond(commonData.getCommonDataList())
                }
                catch (e:Exception){
                    e.stackTrace
                    LOGGER.error("Error while fetching the record :" +  e.stackTrace)
                }


            }

        }

        route("articles") {
            get {

            }
            get("/allArticlesCount"){
                call.respond(daoFacade.getAllArticlesCount())
            }

        }
    }


}
