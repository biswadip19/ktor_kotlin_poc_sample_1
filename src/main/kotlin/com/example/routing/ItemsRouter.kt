package com.example.routing


import com.example.models.Item
import com.example.models.ItemReqeust
import com.example.models.ItemResponse
import com.example.models.ktrom.entity.ItemEntity
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.ktorm.database.Database
import org.ktorm.dsl.*

fun Application.configureItemsRoutingUsingKtorm(ktromHikariDatabaseConn : Database) {
    routing {
        get("/itemsUsingKtrom")
        {
            val items = ktromHikariDatabaseConn.from(ItemEntity).select()
                .map {
                    val itemid=it[ItemEntity.itemid]
                    val name = it[ItemEntity.name]
                    val id = it[ItemEntity.id]
                    Item(itemid ?:-1,name?:"",id?:-1)

                }
            call.respond(items)
        }
        post("/itemsUsingKtrom") {
            val request = call.receive<ItemReqeust>()
            ktromHikariDatabaseConn.insert(ItemEntity) {
                set(it.name, request.name)
                set(it.id,request.id)
            }
        }
        get("/itemsUsingKtrom/{itemId}") {
            val itemid = call.parameters["itemid"]?.toInt() ?: -1
            val items = ktromHikariDatabaseConn.from(ItemEntity).select()
                .where { ItemEntity.itemid eq itemid }
                .map {
                    val itemid = it[ItemEntity.itemid]!!
                    val name = it[ItemEntity.name]!!
                    val id=it[ItemEntity.id]
                    Item(itemid = itemid,name = name,id=id?:-1)
                }.firstOrNull()
            call.respond(
                ItemResponse(items)
            )

        }
    }

}




