package com.example.routing

import com.example.db.DatabaseConnection
import com.example.models.Article
import com.example.models.ktrom.entity.ArticlesEntity
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.ktorm.database.Database
import org.ktorm.dsl.from
import org.ktorm.dsl.map
import org.ktorm.dsl.select


fun Application.configureArticlesRoutingUsingKtorm(ktromHikariCp : Database)
{

   routing {
       get("/ktromArticles")
       {

           val articles=ktromHikariCp.from(ArticlesEntity).select()
               .map {
                   val id=it[ArticlesEntity.id]
                   val title=it[ArticlesEntity.title]
                   val body=it[ArticlesEntity.body]
                   Article(id?:-1,title?:"",body?:"")
               }
           call.respond(articles)
       }

       /*post ("/articles") {
           val request = call.receive<ArticleReqeust>()
           db.insert(ArticlesEntity) {
               set(it.title, request.title)
               set(it.body, request.body)
           }
       }
       get("/articles/{id}"){
           val id= call.parameters["id"]?.toInt()?:-1
           val articles=db.from(ArticlesEntity).select()
               .where { ArticlesEntity.id eq id }
               .map {
                   val id = it[ArticlesEntity.id]!!
                   val title = it[ArticlesEntity.title]!!
                   val body = it[ArticlesEntity.body]!!
                   Article(id = id, title = title, body = body)
               }.firstOrNull()
           call.respond(
               ArticleResponse(articles)
                   )

       }*/



   }

   }

