package com.example

import com.example.config.initEventHubConfig
import com.example.dao.datasource.initKtorHikariDB
import com.example.dao.datasource.initKtromHikariDB
import com.example.di.myModule
import io.ktor.server.application.*
import com.example.plugins.*
import com.example.routing.azureEventHubRouter
import com.example.routing.configureArticlesRoutingUsingKtorm
import com.example.routing.configureCombineRouters
import com.typesafe.config.ConfigFactory
import io.ktor.server.config.*
import org.jetbrains.exposed.sql.Database
import org.koin.core.context.startKoin


fun main(args: Array<String>): Unit =
    io.ktor.server.tomcat.EngineMain.main(args)

@Suppress("unused") // application.conf references the main function. This annotation prevents the IDE from marking it as unused.
fun Application.module() {
    //var ktorHikariDB =  initKtorHikariDB()
    var ktromHikariDB =  initKtromHikariDB()
    var producerClient =  initEventHubConfig()
    //initDBConn()
    startKoin {
        modules(myModule)
    }
    //KotlinCustDataSource.init()
    //KotlinCustDataSource.connect()
    configureSerialization()
    configureMonitoring()
    //configureRouting()
    configureArticlesRoutingUsingKtorm(ktromHikariDB)
    configureCombineRouters(ktromHikariDB)
    azureEventHubRouter(producerClient)
    //callAZEventHub()
    printConfigProperties()
    //configHikariDBConnection()
}

fun configHikariDBConnection() {
    //HikariCustDataSource.init()
}


fun printConfigProperties() {
    val appConfig = HoconApplicationConfig(ConfigFactory.load())
    val dbUrl = appConfig.property("db.jdbcUrl").getString()
    val dbUser =  appConfig.property("db.dbUser").getString()
    val dbPassword = appConfig.property("db.dbPassword").getString()

    println("URL: $dbUrl")
    println("USER: $dbUser")
    println("PASSWORD: $dbPassword")

}






