package com.example.service

import com.example.dao.DAOFacadeImpl
import com.example.models.Article
import com.example.models.ArticleEntity
import org.jetbrains.exposed.sql.SizedIterable
import org.jetbrains.exposed.sql.transactions.transaction
import org.koin.core.component.KoinComponent
import kotlin.time.ExperimentalTime

class ArticleServiceImpl ( private val daoFacade :  DAOFacadeImpl ) : ArticleService,KoinComponent {

   /* override fun allArticles(): List<Article> {
        return daoFacade.allArticles();
    }*/

    override fun getAllArticles(): Iterable<Article> {
        return daoFacade.getAllArticles();
    }

/*    override fun getAllArticles(): SizedIterable<ArticleEntity> {
        return daoFacade.getAllArticles();
    }*/
/*    override fun article(id: Int): Article? =
       transaction {
          // daoFacade.article(id)
       }*/
}