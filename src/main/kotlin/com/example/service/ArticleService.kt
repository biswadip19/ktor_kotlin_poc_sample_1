package com.example.service

import com.example.models.Article
import com.example.models.ArticleEntity
import org.jetbrains.exposed.sql.SizedIterable

interface ArticleService {
    //fun allArticles(): List<Article>
    //fun article(id: Int): Article?
    fun getAllArticles(): Iterable<Article>
    //fun getAllArticles(): SizedIterable<ArticleEntity>
}